class BootstrapLinkRenderer < WillPaginate::ActionView::BootstrapLinkRenderer
  protected
    def html_container(html)
      tag(:ul, html, :class => "pagination")
    end

    def link(text, target, attributes = {})
      if target.is_a? Fixnum
        attributes[:rel] = rel_value(target)
        target = url(target)
      end
      attributes[:href] = target
      # attributes["data-push".to_sym] = "true"
      tag(:a, text, attributes)
    end
end