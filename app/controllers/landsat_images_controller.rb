class LandsatImagesController < ApplicationController
  def new
    @landsat_image = LandsatImage.new
    respond_to do |format|
      format.html
    end
  end

  def create
    @landsat_image = LandsatImage.new(params[:landsat_image])
    @landsat_image.creator_id = current_user.id
    filename = params[:landsat_image_file].original_filename
    @landsat_image.ori_name = filename
    if @landsat_image.save
      directory = "landsat_images_data"
      directory = File.join("..", directory, @landsat_image.id.to_s)
      Dir.mkdir(directory) unless Dir.exist?(directory)

      path = File.join(directory, filename)
      File.open(path, "wb") { |f| f.write(params[:landsat_image_file].read) }
      flash[:notice] = "Landsat image #{filename} successfully uploaded"
    else
      flash[:alert] = "Something's wrong, landsat image #{filename} cann't be uploaded"
    end
    render :inline => ""
  end

  def destroy
    @landsat_image = LandsatImage.find(params[:id])
    identity = @landsat_image.ori_name
    @landsat_image.destroy
    flash[:notice] = "Landsat image #{identity} successfully deleted"
    redirect_to root_path
  end

  def setup_view
    @landsat_image = LandsatImage.find(params[:id])
    respond_to do |format|
      format.html
    end
  end
end
