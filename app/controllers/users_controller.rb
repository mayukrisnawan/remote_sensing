class UsersController < ApplicationController
  def index
     @users = User.paginate(:page => params[:page], :per_page => 5).order("username asc")
     respond_to do |format|
      format.html
    end
  end

  def destroy
    @user = User.find(params[:id])
    identity = @user.username
    @user.destroy
    flash[:notice] = "User #{identity} successfully deleted"
    redirect_to users_path
  end

  def edit_password
    @user = current_user
    respond_to do |format|
      format.html
    end
  end

  def change_password
    @error = false
    @error = "Incorrect Password" if not current_user.valid_password? params[:user][:password]
    @error = "Password confirmation failed, please make sure the password confirmation same as the new password." if params[:user][:new_password] != params[:user][:password_confirmation]
    
    if not @error
      current_user.password = params[:user][:new_password]
      if current_user.save
        sign_in current_user, :bypass => true
        flash[:notice] = "Password successfully changed."
      else
        flash[:alert] = "Error. Password cannot be changed. #{current_user.errors.full_messages.join(",")}"
      end
    end
    respond_to do |format|
      format.js
    end
  end

  def edit_email_address
    respond_to do |format|
      format.html
    end
  end

  def change_email_address
    current_user.email = params[:email]
    if current_user.save
      sign_in current_user, :bypass => true
      flash[:notice] = "Email successfully changed."
    else
      flash[:alert] = "Error. Email cannot be changed. #{current_user.errors.full_messages.join(",")}"
    end
    respond_to do |format|
      format.js
    end
  end

  def change_level
    @user = User.find(params[:id])
    @success = @user.change_level(params[:new_level])
    respond_to do |format|
      format.js
    end
  end
end
