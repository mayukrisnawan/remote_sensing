class PreviewsController < ApplicationController
  def new
    @landsat_image = LandsatImage.find(params[:landsat_image_id])
    respond_to do |format|
      format.html
    end
  end

  def create
    @landsat_image = LandsatImage.find(params[:preview][:landsat_image_id])
    @preview = Preview.new(params[:preview])
    @preview_id = @preview.create_preview!
    respond_to do |format|
      format.js
    end
  end

  def show
    @landsat_image = LandsatImage.find(params[:landsat_image_id])
    @preview = Preview.find(params[:id])
    respond_to do |format|
      format.html
    end
  end

  def view_image
    @preview = Preview.find(params[:id])
    send_file @preview.preview_file_path, :type => 'image/jpeg', :disposition => 'inline'
  end
end
