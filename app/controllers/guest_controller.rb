class GuestController < ApplicationController
  def index
     @landsat_images = LandsatImage.paginate(:page => params[:page], :per_page => 5).order("created_at desc")
     respond_to do |format|
      format.html
    end
  end
end
