class User < ActiveRecord::Base
  devise :database_authenticatable,
				 :rememberable, :trackable, :validatable, :registerable

  attr_accessible :username, :password, :email, :password_confirmation, :remember_me
  has_many :landsat_images, :foreign_key => :creator_id, :dependent => :destroy

  def change_level(new_level_name)
    if new_level_name == "member"
      new_level = 0
    elsif new_level_name == "admin"
      new_level = 1
    end
    update_attribute(:level, new_level)
  end

  def admin?
    level_name == "admin"
  end

  def level_name
    if level == 0
      return "member"
    elsif level == 1
      return "admin"
    end
  end
end
