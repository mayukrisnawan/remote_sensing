class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    if user.admin?
        can :manage, :all
    else
        can :create, LandsatImage
        can [:create, :delete], Preview
        can :delete, LandsatImage, :creator_id => user.id
        can :read, :all
    end
  end
end
