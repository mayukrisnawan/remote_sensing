class LandsatImage < ActiveRecord::Base
  attr_accessible :name, :ori_name
  before_destroy :remove_files
  belongs_to :creator, class_name: 'User'

  def data_path
    base = File.join("..", "landsat_images_data")
    landsat_image_path = File.join(base, id.to_s)
  end

  def remove_files
    puts ">>>>>>>>>>>>>>>>>>> DELETE : #{data_path}"
    FileUtils.rm_rf data_path
  end
end
