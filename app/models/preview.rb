class Preview < ActiveRecord::Base
  belongs_to :landsat_image
  attr_accessible :quality, :landsat_image_id, :interpolation
  before_destroy :remove_files

  def create_preview!
    old_previews = Preview.where("quality = ? AND landsat_image_id = ? AND interpolation = ?", quality, landsat_image_id, interpolation)
    if not old_previews.empty?
      if File.exist? old_previews.first.preview_file_path
        return old_previews.first.id
      else
        old_previews.first.destroy
      end
    end

    landsat_image = LandsatImage.find(landsat_image_id)
    base = File.join("..", "landsat_images_data")
    landsat_image_path = File.join(base, landsat_image.id.to_s)

    if save
      preview_path = File.join(landsat_image_path, "preview_#{id}")
      Dir.mkdir(preview_path) unless Dir.exist?(preview_path)
      preview_file_location = File.join(preview_path, "preview.jpg")
      geotiff_source_location = File.join(landsat_image_path, landsat_image.ori_name)

      command = "python view_creation.py --source #{geotiff_source_location} --output #{preview_file_location} --quality #{quality}"
      puts ">>>>>>>>>>>"
      puts command
      system command
      puts ">>>>>>>>>>>>>>>>>>>>>>> COMPLETE."
      return id
    end
    return false
  end

  def preview_file_path
    landsat_image = LandsatImage.find(landsat_image_id)
    preview_path = File.join(landsat_image.data_path, "preview_#{id}")
    File.join(preview_path, "preview.jpg")
  end

  def geo_transform
    landsat_image = LandsatImage.find(landsat_image_id)
    xml_path = File.join(landsat_image.data_path, "tmp", "ori.jpg.aux.xml")
    xml_string = File.read(xml_path)
    hash = Hash.from_xml(xml_string)
    geo_transform_data = hash["PAMDataset"]["GeoTransform"].split(",").map(&:to_f)
    result = {}
    result[:west] = geo_transform_data[0].to_i
    result[:north] = geo_transform_data[3].to_i
    return result
  end

  def projection
    landsat_image = LandsatImage.find(landsat_image_id)
    xml_path = File.join(landsat_image.data_path, "tmp", "ori.jpg.aux.xml")
    xml_string = File.read(xml_path)
    hash = Hash.from_xml(xml_string)
    projection_data = hash["PAMDataset"]["SRS"]
    begin
      /"(.+)",GEOGCS/.match(projection_data)[1]
    rescue
      "-"
    end
  end

  def interpolation_name
    methods = ["BILINEAR", "BICUBIC", "NEAREST NEIGHBOR", "LANCOZ"]
    methods[interpolation]
  end

  def remove_files
    landsat_image = LandsatImage.find(landsat_image_id)
    preview_path = File.join(landsat_image.data_path, "preview_#{id}")
    puts ">>>>>>>>>>>>>>>>>>> DELETE : #{preview_path}"
    FileUtils.rm_rf preview_path
  end
end
