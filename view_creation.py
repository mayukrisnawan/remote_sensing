from osgeo import gdal
import numpy
import os
import argparse
import shutil
import cv2

parser = argparse.ArgumentParser(description="Produce JPG for Landsat Image (GeoTIFF)")
parser.add_argument("--source", help="GeoTIFF source")
parser.add_argument("--output", default="./out.jpg", help="Output File")
parser.add_argument("--quality", default="100", help="Pixel Density of Output")
parser.add_argument("--interpolation_method", default="0", help="Interpolation Method")
args = parser.parse_args()

dataset = gdal.Open(args.source)
cols = dataset.RasterXSize
rows = dataset.RasterYSize
bands = dataset.RasterCount
driver = dataset.GetDriver()

geotransform = dataset.GetGeoTransform()
projection = dataset.GetProjection()

landsat_image_directory = os.path.dirname(args.source)
tmp_dir = os.path.join(landsat_image_directory, "tmp")
if not os.path.isdir(tmp_dir):
  os.mkdir(tmp_dir)
tmp_file = os.path.join(tmp_dir, "output.tmp")
ori_jpg_file =  os.path.join(tmp_dir, "ori.jpg")

if not os.path.isfile(ori_jpg_file):
  band = dataset.GetRasterBand(1)
  data = band.ReadAsArray(0, 0, dataset.RasterXSize, dataset.RasterYSize)

  # tipe data landsat 8 adalah UInt16 (2 byte, nilai maksimalnya adalah 65535), 
  # sedangkan JPG hanya bisa menampung nilai 0-255
  # jadi, konversi warna ke UInt8 (1 byte)
  if band.DataType == gdal.GDT_UInt16:
    data *= 255.0/65535.0

  tmp_output = driver.Create(tmp_file, cols, rows, bands, gdal.GDT_Byte)
  tmp_output.SetGeoTransform(geotransform)
  tmp_output.SetProjection(projection)
  tmp_output.GetRasterBand(1).WriteArray(data)

  JPEGdriver = gdal.GetDriverByName('PNG')
  JPEGdriver.CreateCopy(ori_jpg_file, tmp_output, 0)

  data = None
  dataset = None
  tmp_output = None
  driver.Delete(tmp_file)

# Scaling gambar dengan openCV
quality = float(args.quality)
if quality == 1.0:
  shutil.copy(ori_jpg_file, args.output)
else:
  ori_image = cv2.imread(ori_jpg_file)
  interpolation_method = cv2.INTER_LINEAR
  if args.interpolation_method == 0:
    interpolation_method = cv2.INTER_LINEAR
  elif args.interpolation_method == 1:
    interpolation_method = cv2.INTER_CUBIC
  elif args.interpolation_method == 2:
    interpolation_method = cv2.INTER_NEAREST
  elif args.interpolation_method == 3:
    interpolation_method = cv2.INTER_LANCZOS4
  scaled_image = cv2.resize(ori_image, None, fx=quality, fy=quality, interpolation=interpolation_method)
  cv2.imwrite(args.output, scaled_image)