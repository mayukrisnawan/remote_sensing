RemoteSensing::Application.routes.draw do
  devise_for :users
  root :to => 'guest#index'
  resources :landsat_images do
    resources :previews, only: [:new, :create, :show] do
      member do
        get 'view_image' => "previews#view_image", :as => "view_image"
      end
    end
  end
  resources :users do
    collection do
      get 'edit_password' => "users#edit_password", :as => "edit_password"
      post 'change_password' => "users#change_password", :as => "change_password"
      get 'edit_email_address' => "users#edit_email_address", :as => "edit_email_address"
      post 'change_email_address' => "users#change_email_address", :as => "change_email_address"
      post 'change_level' => "users#change_level", :as => "change_level"
    end
  end
end
