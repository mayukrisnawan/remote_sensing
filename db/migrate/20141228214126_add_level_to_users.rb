class AddLevelToUsers < ActiveRecord::Migration
  def change
    add_column :users, :level, :integer, :limit => 1, :default => 0
  end
end
