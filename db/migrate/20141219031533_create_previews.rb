class CreatePreviews < ActiveRecord::Migration
  def change
    create_table :previews do |t|
      t.float :quality
      t.references :landsat_image

      t.timestamps
    end
    add_index :previews, :landsat_image_id
  end
end
