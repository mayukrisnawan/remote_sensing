class CreateLandsatImages < ActiveRecord::Migration
  def change
    create_table :landsat_images do |t|
      t.string :name
      t.string :ori_name

      t.timestamps
    end
  end
end
