class AddCreatorIdToLandsatImages < ActiveRecord::Migration
  def change
    add_column :landsat_images, :creator_id, :integer
    LandsatImage.update_all(:creator_id => 1)
  end
end
