class AddInterpolationToPreviews < ActiveRecord::Migration
  def change
    add_column :previews, :interpolation, :integer, :limit => 1
  end
end
